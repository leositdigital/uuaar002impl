package com.bbva.uuaa.lib.r002.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.apx.exception.db.NoResultException;
import com.bbva.apx.exception.db.TimeoutException;
import com.bbva.uuaa.dto.cuentas.AccountDTO;
import com.bbva.uuaa.dto.cuentas.pagination.PaginationInDTO;
import com.bbva.uuaa.dto.cuentas.pagination.PaginationOutDTO;

/**
 * The UUAAR002Impl class...
 */
public class UUAAR002Impl extends UUAAR002Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(UUAAR002Impl.class);

	@Override
	public int executeCreateAccount(AccountDTO accountIn) {
		Map<String , Object> args = new HashMap<String, Object>();
		//INSERT INTO USERAXITY.ACCOUNT (CDDIVISA, NUCUENTA, CDTIPOCUENTA, TOIMPORTE) VALUES (:cdDivisaIn, :nuCuentaIn, :cdCuentaIn, :toImporteIn);
		args.put("cdDivisaIn", accountIn.getCdDivisa());
		args.put("nuCuentaIn", accountIn.getNuCuenta());
		args.put("cdCuentaIn", accountIn.getCdTpCuenta());
		args.put("toImporteIn", accountIn.getToImporte());
		LOGGER.info("@@@ ANTES DEL INSERT {}", args);
		
		int result = this.jdbcUtils.update("SQL.INSERT.ACCOUNT", args);
		
		LOGGER.info("@@@ Después DEL INSERT {}", result);
		
		return result;
	}

	@Override
	public List<AccountDTO> executeReadAccount(AccountDTO accountIn,PaginationInDTO paginationIn) {
		List<AccountDTO> response = new ArrayList();
		List<Map<String,Object>> accounts = new ArrayList();
		Map<String , Object> args = new HashMap<String, Object>();
		args.put("cdCuentaIn", accountIn.getCdTpCuenta());		
		int pagKey = paginationIn.getPaginationKey();
		int pagSize= paginationIn.getPageSize();
		
		int firsRow = pagKey==1?pagKey:pagSize *(pagKey-1)+1;
		
		//this.jdbcUtils.addQuery(query, "SQL.SELECT.ACCOUNT");
		try {	   
			accounts = this.jdbcUtils.pagingQueryForList("SQL.SELECT.ACCOUNT", firsRow, pagSize, args);
			
		}catch(NoResultException ex) { //uso de Exception y RuntimeException no está permitido
			LOGGER.info("@@@ NoResultException {}", ex);
			this.addAdvice("UUAA00000203");	
		}
		
		if(accounts.size()>0) {
		   for (Map<String,Object> row :accounts){			
			AccountDTO accountOut = new AccountDTO ();
			accountOut.setCdDivisa(row.get("CDDIVISA").toString());
			accountOut.setCdTpCuenta(row.get("CDTIPOCUENTA").toString());
			accountOut.setNuCuenta(row.get("NUCUENTA").toString());
			//Long importe = new Long(Integer.parseInt(row.get("TOIMPORTE").toString()));
			accountOut.setToImporte(Integer.parseInt(row.get("TOIMPORTE").toString()));			
			response.add(accountOut);			
		  }
		}else {
			this.addAdvice("UUAA00000202");			
		}
		
		return response;
	}

	@Override
	public int executeUpdateAccount(AccountDTO accountIn) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int executeDeleteAccount(AccountDTO accountIn) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public PaginationOutDTO executeGetPaginationOut(AccountDTO accountIn, PaginationInDTO paginationIn) {
		
		PaginationOutDTO responseDTO = new PaginationOutDTO();
		Map<String , Object> args = new HashMap<String, Object>();
		args.put("tpCuentaIn", accountIn.getCdTpCuenta());
		int total = this.jdbcUtils.queryForInt("SQL.COUNT.ACCOUNT", args);
		LOGGER.info("@@@@@@ Total de registros {} ",total);
		String moreData = (paginationIn.getPaginationKey() * paginationIn.getPageSize()) < total ?"Y":"N";
		responseDTO.setHasMoreData(moreData);
		int nextPage = moreData.equals("Y")?paginationIn.getPaginationKey()+1:paginationIn.getPaginationKey();
		responseDTO.setPaginationKey(nextPage);
		
		return responseDTO;
	}




}
