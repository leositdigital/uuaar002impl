package com.bbva.uuaa.lib.r002;

import com.bbva.apx.exception.db.NoResultException;
import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import com.bbva.elara.utility.jdbc.JdbcUtils;
import com.bbva.uuaa.dto.cuentas.AccountDTO;
import com.bbva.uuaa.dto.cuentas.pagination.PaginationInDTO;
import com.bbva.uuaa.dto.cuentas.pagination.PaginationOutDTO;

import java.util.List;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/UUAAR002-app.xml",
		"classpath:/META-INF/spring/UUAAR002-app-test.xml",
		"classpath:/META-INF/spring/UUAAR002-arc.xml",
		"classpath:/META-INF/spring/UUAAR002-arc-test.xml" })
public class UUAAR002Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(UUAAR002Test.class);

	@Spy
	private Context context;

	@Resource(name = "uuaaR002")
	private UUAAR002 uuaaR002;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;
	
	@Resource(name="jdbcUtils")
	private JdbcUtils jdbcUtils; 

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		context = new Context();
		ThreadContext.set(context);
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.uuaaR002;
		if(this.uuaaR002 instanceof Advised){
			Advised advised = (Advised) this.uuaaR002;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}
	
	@Test
	public void executeTest(){
		LOGGER.info("Executing the test...");
		AccountDTO accountIn = new AccountDTO();
		accountIn.setCdTpCuenta("NO");
		PaginationInDTO paginationIn = new PaginationInDTO();
		paginationIn.setPageSize(10);
		paginationIn.setPaginationKey(1);
		Mockito.doReturn(50).when(jdbcUtils).queryForInt(Mockito.anyString(), Mockito.anyMap());
		PaginationOutDTO resulTest = uuaaR002.executeGetPaginationOut(accountIn, paginationIn);
		Assert.assertEquals("Y", resulTest.getHasMoreData());
	}
	
	@Test
	public void executeTestException(){
		LOGGER.info("Executing the test...");
		AccountDTO accountIn = new AccountDTO();
		accountIn.setCdTpCuenta("NO");
		PaginationInDTO paginationIn = new PaginationInDTO();
		paginationIn.setPageSize(10);
		paginationIn.setPaginationKey(1);
		Mockito.doThrow(new NoResultException("NoResultException")).when(jdbcUtils).pagingQueryForList(Mockito.eq("SQL.SELECT.ACCOUNT"),Mockito.anyInt(),Mockito.anyInt(), Mockito.anyMap());  
		List<AccountDTO> resulTest = uuaaR002.executeReadAccount(accountIn, paginationIn);
		Assert.assertEquals(0, resulTest.size());
	}
	
}
